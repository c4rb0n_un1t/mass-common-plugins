Common MASS plugins compatible with https://gitlab.com/c4rb0n_un1t/MASS-Plugins

Plugins description:
- Core - start first and init all other plugins
- UIManager - provide mechanism for managing UI plugins
- PluginLinker - setup links between all plugins
- NotificationManager - set timers and send crossplatform notifications
- ExtendableDataManager - provide extendable mechanism for data management for all plugins
- DataBase - interact with data base
