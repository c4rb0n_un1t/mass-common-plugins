#include "core.h"

Core::Core() :
	QObject(nullptr),
	PluginBase(this)
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IApplication), this}
	});
}

Core::~Core()
{
}

void Core::coreInit(quint32 corePluginUID, QObject* appObject, QWeakPointer<IApplication> app)
{
	m_app = app;

	connect(appObject, SIGNAL(onUserAnswered(quint32, quint16)), this, SIGNAL(onUserAnswered(quint32, quint16)));

	IPluginHandlerPtr handler;
	for(auto& plugin : app.toStrongRef()->getPlugins())
	{
		if(plugin.toStrongRef()->getUID() == corePluginUID)
		{
			handler = plugin;
		}
	}

	bool isInited = pluginInit(handler.toStrongRef()->getUID(), handler.toStrongRef()->getMeta());
	Q_ASSERT(isInited);

	m_linker.reset(new SimpleLinker(this, getDescriptor(), appObject, app));
	m_linker->init();
}

bool Core::coreFini()
{
	return true;
}

void Core::onReady()
{
}

QWidget *Core::getParentWidget()
{
	return m_app.toStrongRef()->getParentWidget();
}

const QVector<IPluginHandlerPtr> &Core::getPlugins()
{
	return m_app.toStrongRef()->getPlugins();
}

IPluginHandlerPtr Core::makePluginHandler(const QString &path)
{
	return m_app.toStrongRef()->makePluginHandler(path);
}

quint32 Core::askUser(const QString& question, const QVariantList& options)
{
	return m_app.toStrongRef()->askUser(question, options);
}
