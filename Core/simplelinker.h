#pragma once


#include "../../../Application/Interfaces/iapplication.h"
#include "../../Interfaces/Architecture/iplugin.h"
#include "../../Interfaces/Architecture/referenceinstance.h"

class SimpleLinker : public QObject
{
	Q_OBJECT
public:
	SimpleLinker(QObject* parent, IReferenceDescriptorPtr coreDescr, QObject* appObject, QWeakPointer<IApplication> app);
	virtual ~SimpleLinker() override;

public:
	void init();
	ReferenceInstancePtr<IPlugin> addPlugin(IPluginHandlerPtr plugin);

private:
	typedef QPair<IPluginHandlerPtr, ReferenceInstancePtr<IPlugin>> LoadedPluginPair;

private slots:
	void onUserAnswered(quint32 askId, quint16 optionIndex);

private:
	void linkPlugins(IPluginHandlerPtr plugin);

private:
	IReferenceDescriptorPtr m_coreDescr;
	QObject* m_appObject;
	QWeakPointer<IApplication> m_app;
	QMap<Interface, QList<QWeakPointer<LoadedPluginPair>>> m_pluginsInterfaces;
	QList<QSharedPointer<LoadedPluginPair>> m_plugins;
	quint32 m_conflictAskId;
	QList<QPair<QString, IPluginHandlerPtr>> m_conflictedLinkerPlugins;
};

