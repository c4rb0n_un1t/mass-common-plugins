TEMPLATE = subdirs

SUBDIRS += Core \
	DataBase \
	ExtendableDataManager \
	NotificationManager \
	PluginLinker \
	UIManager
