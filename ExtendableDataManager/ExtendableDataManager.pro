#-------------------------------------------------
#
# Project created by QtCreator 2017-02-01T20:08:55
#
#-------------------------------------------------
TARGET = ExtendableDataManagerPlugin
TEMPLATE = lib
QT += core sql widgets

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

SOURCES += \
    model.cpp \
    modelfilter.cpp \
    plugin.cpp \
    tablehandler.cpp \
    item.cpp

HEADERS += \
    ../../Interfaces/Middleware/idatabase.h \
    ../../Interfaces/Middleware/iextendabledatamodel.h \
    itablehandler.h \
    model.h \
    modelfilter.h \
    plugin.h \
    tablehandler.h \
    item.h

DISTFILES += \
    PluginMeta.json
