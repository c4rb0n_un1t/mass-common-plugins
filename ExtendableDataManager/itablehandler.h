#pragma once

#include <QtCore>

#include "../../Interfaces/Middleware/idataextention.h"

typedef QMap<QString, QVariant> TableStructMap;

struct TableItem
{
	int id;
	QMap<Interface, TableStructMap> dataChunks;
};

class ITableHandler
{
public:
	virtual bool SetRelation(Interface relationInterface, TableStructMap fields) = 0;
	virtual bool DeleteRelation(Interface relationInterface) = 0;

	virtual int AddItem(TableItem& item) = 0;
	virtual bool UpdateItem(TableItem& item) = 0;
	virtual bool DeleteItem(int id) = 0;

	virtual QList<TableItem> GetData() = 0;
	virtual TableItem GetItem(int id) = 0;
	virtual QPointer<IExtendableDataModel> GetModel() = 0;
	virtual QMap<Interface, TableStructMap> GetHeader() = 0;
};
