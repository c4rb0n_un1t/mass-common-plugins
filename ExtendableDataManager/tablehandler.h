#pragma once


#include <QMap>
#include <QtSql>
#include <QDebug>
#include <QVariant>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Middleware/idatabase.h"
#include "model.h"
#include "itablehandler.h"

//! \addtogroup ExtendableDataManager_imp
//!  \{
class TableHandler : public ITableHandler
{
public:
	QMap<QVariant::Type, QString> dataBaseTypesNames =
	{
		{QVariant::Int, "INTEGER"},
		{QVariant::UInt, "INTEGER"},
		{QVariant::LongLong, "INTEGER"},
		{QVariant::ULongLong, "INTEGER"},
		{QVariant::String, "VARCHAR"},
		{QVariant::DateTime, "INTEGER"},
		{QVariant::ByteArray, "BLOB"},
		{QVariant::Bool, "BOOLEAN"}
	};

	TableHandler(ReferenceInstancePtr<IDataBase> dataSource, Interface mainInterface);
	virtual ~TableHandler();

	inline QString TableName()
	{
		return tableName;
	}
	bool HasRelation(Interface relationInterface);

	bool CreateTable();
	bool SetRelation(Interface relationInterface, TableStructMap fields) override;
	bool DeleteRelation(Interface relationInterface) override;

	int AddItem(TableItem& item) override;
	bool UpdateItem(TableItem& item) override;
	bool DeleteItem(int id) override;

	QList<TableItem> GetData() override;
	TableItem GetItem(int id) override;
	QPointer<IExtendableDataModel> GetModel() override;
	QMap<Interface, TableStructMap> GetHeader() override;

private:
	ReferenceInstancePtr<IDataBase> dataSource;
	ExtendableDataModel *itemModel;
	QString tableName;
	bool isCreated;

	TableStructMap wholeTableStruct;
	TableStructMap coreTableStruct;
	QMap<Interface, TableStructMap> relationTablesStructs;

	QString GetInsertValuesString(TableStructMap &tableStruct, int id, TableStructMap& itemData);
	QString GetUpdateValuesString(TableStructMap &tableStruct, int id);
	QString GetUpdateValuesString(TableStructMap &tableStruct, int id, TableStructMap& itemData);

	bool IsDataSourceExists();
	bool IsTableExists(QString tableName);
	bool IsTableHasRightStructure(QString tableName, TableStructMap &tableStruct);
	void CombineWholeTableStruct();
	QString GetHeaderString(TableStructMap &tableStruct, bool createRelation = false);
	QString GetFieldsNames(QString tableName, TableStructMap &tableStruct, bool includeId = false);
	void InstallModel();
	QString convertIterfaceToTableName(const Interface& interface);
};
//!  \}

